# Introduction
This project aims to normalise conversations around sexual consent using gamification by creating content in the vernacular. The project is outlined at [Kansent.co](https://kansent.co/)
# Minimum Viable Product
A mobile app that gamifies the concept of sexual consent to encourage partners make healthy and consensual choices
# Privacy centric
* General Data Protection Regulation (GDPR) compliant
* No meta-data stored on servers to protect identity of users of the service
